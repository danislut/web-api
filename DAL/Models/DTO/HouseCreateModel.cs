﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models.DTO
{
    public class HouseCreateModel
    {
        public string Address { get; set; }
        public string Name { get; set; }

        public string UserName { get; set; }
    }
}
