﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models.DTO
{
    public class HouseWaterSumModel
    {
        public int HouseId { get; set; }
        public string Address { get; set; }
        public string Name { get; set; }
        public ulong Sum { get; set; }
    }
}
