﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using BL.Services;
using Microsoft.EntityFrameworkCore;

namespace Web_API
{

    class Program
    {   
        static void Main(string[] args)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                
            }
            Console.Read();
        }
    }
    public class House
    {
        public int Id { get; set; }
        [Required]
        public string Address { get; set; }
        public string Name { get; set; }
        public List<Water> Waters { get; set; }
    }
    public class Water
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(15)]
        public string Number { get; set; }
        public int Reading { get; set; }
        public int? HouseId { get; set; }
        [ForeignKey("HouseId")]
        public House House { get; set; }
    }
    public class ApplicationContext: DbContext
    {
        public DbSet<House> Houses { get; set; }
        public DbSet<Water> Waters { get; set; }
        public void CreateHouse(string address, string name)
        {
            foreach(var h in Houses)
                if (h.Name == name)
                {
                    Console.WriteLine("Данный адрес уже существует");
                    return;
                }
            Houses.Add(new House { Name = name, Address = address });
            SaveChanges();
        }
        public void DeleteHouse(int id)
        {
            Houses.Remove(Houses.Find(id));
        }
        public void FindHouse(int id)
        {
            Houses.Find(id);
            SaveChanges();
        }
        public void EditHouse(int id, string address, string name)
        {
            House editHouse = Houses.Find(id);
            editHouse.Address = address;
            editHouse.Name = name;
            SaveChanges();
        }
        public List<House> AllHouses()
        {
            return Houses.ToList();
            //foreach (var house in Houses)
            //    Console.WriteLine($"{house.Id}.\t{house.Address}\t{house.Name}");
        }
        public void CreateWater(string num, int reading)
        {
            if (num.Length > 15)
            {
                Console.WriteLine("Слишком большая длина номера счетчика");
                return;
            }
            if (reading < 0)
            {
                Console.WriteLine("Показания счетчика не могут быть отрицательными");
                return;
            }
            foreach(var w in Waters)
                if (w.Number == num)
                {
                    Console.WriteLine("Данный серийный номер уже существует");
                    return;
                }
            Waters.Add(new Water { Number = num, Reading = reading });
            SaveChanges();
        }
        public void RegisterWater(int waterId, int houseId)
        {
            Water regWater = Waters.Find(waterId);
            //Houses.Include(w => w.Waters).FirstOrDefaultAsync();
            regWater.House = Houses.Find(houseId);
            regWater.HouseId = houseId;
            //Houses.Find(houseId).Waters.Add(regWater);
            SaveChanges();
        }
        //public House MostConsumption()
        //{
        //    House result;
        //    foreach (var h in Houses)
        //    {
        //        int Consum = 0;
        //        foreach (var w in h.Waters)
        //            Consum += w.Reading;

        //    }
        //}
        //public House LeastConsumption()
        //{

        //}
        public List<Water> AllHousesWater(House house)
        {
            return house.Waters.ToList();
        }
        public void AddWaterDataByNum(string num, int reading)
        {
            var waters = (from w in Waters.Include(h => h.House.Waters)
                          where w.Number == num
                          select w).ToList();
            foreach(var w in waters)
                Waters.Add(w);
            SaveChanges();
        }
        public void AddWaterDataById(int id, int reading)
        {
            Waters.Find(id).Reading = reading;
            SaveChanges();
        }
        public ApplicationContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<House>().HasIndex(h => h.Address).IsUnique();
            modelBuilder.Entity<Water>().HasIndex(w => w.Number).IsUnique();
            modelBuilder.Entity<Water>()
            .HasOne(w => w.House)
            .WithMany(h => h.Waters)
            .OnDelete(DeleteBehavior.Cascade);
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=myserver;Username=mylogin;Password=mypass;Database=mydatabase");
        }
    }
}
