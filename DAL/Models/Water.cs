﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Models
{
    public class Water
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Укажите заводской номер")]
        [MaxLength(15, ErrorMessage = "Заводской номер не более 15 символов")]
        [Index(IsUnique = true)]
        public string Number { get; set; }
        public uint Reading { get; set; }
        public int? HouseId { get; set; }
        [JsonIgnore]
        [ForeignKey("HouseId")]
        public House House { get; set; }

        public bool IsValid()
        {
            return (this.Number != null && this.Number.Length <= 15);
        }
    }
}
