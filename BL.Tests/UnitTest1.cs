using BL.Interfaces;
using DAL.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebAPI.Controllers;

using Microsoft.EntityFrameworkCore;
using BL.Services;
using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore.Storage;
using System.Linq;
using DAL.Models.DTO;
using System.ComponentModel.DataAnnotations;

namespace BL.Tests
{
    [TestClass]
    public class UnitTest1
    {
        DatabaseContext _db;
        IHouseService _houseService;
        IWaterService _waterService;

        public UnitTest1()
        {
            var options = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "TestDb2", new InMemoryDatabaseRoot())
                .Options;
            _db = new DatabaseContext(options);
            _houseService = new HouseService(_db);
            _waterService = new WaterService(_db);
            _db.Houses.AddRange(InitHouseList());
            _db.Waters.AddRange(InitWaterList());
        }

        private List<House> InitHouseList()
        {
            var result = new List<House>();
            for (int i = 1; i < 10; i++)
                result.Add(new House { Address = $"�����������, {i}", Name = "Danfoss" });
            return result;
        }
        private List<Water> InitWaterList()
        {
            var result = new List<Water>();
            for (int i = 1; i < 5; i++)
                _waterService.CreateWater(new Water { Number = $"AAA-{i}" });
            return result;
        }

        [TestMethod]
        public void CreateHouseNull()
        {
            Assert.ThrowsException<NullReferenceException>(() => _houseService.CreateHouse(null));
        }
        [TestMethod]
        public void CreateHouse()
        {
            var house = new House { Address = $"�����������, 10", Name = "Danfoss" };
            var result = _houseService.CreateHouse(house);
            Assert.AreEqual($"House {house.Address} created", result);
        }
        [TestMethod]
        public void CreateHouseWithoutAddress()
        {
            var house = new House { Name = "Danfoss" };
            //var result = _houseService.CreateHouse(house);
            //Assert.AreEqual("������� �����", result);
            Assert.ThrowsException<ArgumentNullException>(() => _houseService.CreateHouse(house));
        }
        [TestMethod]
        public void CreateHouseWithSameAddress()
        {
            var house = new House { Address = $"�����������, 3", Name = "Company" };
            Assert.ThrowsException<ArgumentException>(() => _houseService.CreateHouse(house));
        }
        [TestMethod]
        public void GetAllHouses()
        {
            int expCount = 9;
            Assert.AreEqual(expCount, ((List<House>)_houseService.GetAllHouses()).Count);
        }
        [TestMethod]
        public void DeleteHouse()
        {
            var house = new House { Address = $"�����������, 11", Name = "Danfoss" };
            _houseService.CreateHouse(house);
            var result = _houseService.DeleteHouseById(11);
            Assert.AreEqual($"House {house.Address} deleted", result);
        }
        [TestMethod]
        public void DeleteWrongHouse()
        {
            int wrongId = 22;
            Assert.ThrowsException<KeyNotFoundException>(() => _houseService.DeleteHouseById(wrongId));
        }
        [TestMethod]
        public void EditHouseFull()
        {
            string address = "�����������, 1�";
            string name = "Company";
            var model = new HouseEditModel { HouseId = 1, Address = address, Name = name };
            var result = _houseService.EditHouseById(model);
            Assert.AreEqual($"Updated house id = {model.HouseId} address = {model.Address} name = {model.Name}", result);
        }
        [TestMethod]
        public void EditHouseAddress()
        {
            string address = "�����������, 1�";
            var model = new HouseEditModel { HouseId = 1, Address = address };
            var result = _houseService.EditHouseById(model);
            Assert.AreEqual($"Updated house id = {model.HouseId} address = {model.Address}", result);
        }
        [TestMethod]
        public void EditHouseName()
        {
            string name = "Company";
            var model = new HouseEditModel { HouseId = 1, Name = name };
            var result = _houseService.EditHouseById(model);
            Assert.AreEqual($"Updated house id = {model.HouseId} name = {model.Name}", result);
        }
        [TestMethod]
        public void EditHouseNull()
        {
            Assert.ThrowsException<NullReferenceException>(() => _houseService.EditHouseById(null));
        }
        [TestMethod]
        public void EditHouseWrongId()
        {
            string address = "�����������, 1�";
            string name = "Company";
            var model = new HouseEditModel { HouseId = 20, Address = address, Name = name };
            Assert.ThrowsException<KeyNotFoundException>(() => _houseService.EditHouseById(model));
        }
        [TestMethod]
        public void GetHouse()
        {
            int id = 8;
            var result = _houseService.GetHouseById(id);
            //Assert.AreEqual(id, result.Id);
            Assert.AreEqual(_db.Houses.Find(id), result);
        }
        [TestMethod]
        public void GetHouseWrongId()
        {
            int id = 20;
            Assert.ThrowsException<KeyNotFoundException>(() => _houseService.GetHouseById(id));
        }
        public void RegisterWater(int waterId, int houseId)
        {
            var water = _db.Waters.Find(waterId);
            var house = _db.Houses.Find(houseId);
            house.Waters = new List<Water> { water };
            //house.Waters.Add(water);
            water.HouseId = houseId;
            water.House = house;
            //_db.SaveChanges();
        }
        //[TestMethod]
        //public void GetHouseWaters()
        //{
        //    int houseId = 1;
        //    RegisterWater(1, houseId);
        //    var result = (List<WaterReturnModel>)_houseService.GetHouseWaters(houseId);
        //    Assert.AreEqual(1, result.Count);
        //    Assert.AreEqual(_db.Waters.Find(1), result[0]);
        //}
        [TestMethod]
        public void GetHouseWatersWrongId()
        {
            RegisterWater(1, 1);
            Assert.ThrowsException<KeyNotFoundException>(() => _houseService.GetHouseWaters(20));
        }
        [TestMethod]
        public void AddWaterDataById()
        {
            int id = 1;
            uint reading = 200;
            var result = _waterService.AddWaterDataById(id, reading);
            Assert.AreEqual($"Water meter id = {id} updated (reading = {reading})", result);
        }
        [TestMethod]
        public void AddWaterDataByWrongId()
        {
            int id = 20;
            uint reading = 200;
            Assert.ThrowsException<KeyNotFoundException>(() => _waterService.AddWaterDataById(id, reading));
        }
        [TestMethod]
        public void AddWaterDataByNum()
        {
            string num = "AAA-1";
            uint reading = 200;
            var result = _waterService.AddWaterDataByNum(num, reading);
            Assert.AreEqual($"Water meter number = {num} updated (reading = {reading})", result);
        }
        [TestMethod]
        public void AddWaterDataByWrongNum()
        {
            string num = "WWW-1";
            uint reading = 200;
            Assert.ThrowsException<KeyNotFoundException>(() => _waterService.AddWaterDataByNum(num, reading));
        }
        [TestMethod]
        public void CreateWater()
        {
            var water = new Water { Number = $"AAA-5" };
            var result = _waterService.CreateWater(water);
            Assert.AreEqual($"Water meter {water.Number} created", result);
        }
        [TestMethod]
        public void CreateWaterWrongLength()
        {
            var water = new Water { Number = "12345678901234567890" };
            Assert.ThrowsException<ArgumentNullException>(() => _waterService.CreateWater(water));
        }
        [TestMethod]
        public void CreateWaterWithoutNumber()
        {
            var water = new Water { Reading = 100500 };
            Assert.ThrowsException<ArgumentNullException>(() => _waterService.CreateWater(water));
        }
        [TestMethod]
        public void CreateWaterWithSameName()
        {
            var water = new Water { Number = "AAA-4", Reading = 400 };
            Assert.ThrowsException<ArgumentException>(() => _waterService.CreateWater(water));
        }
        [TestMethod]
        public void RegisterWater()
        {
            int waterId = 1;
            int houseId = 1;
            var result = _waterService.RegisterWater(waterId, houseId);
            Assert.AreEqual("Water meter registered", result);
            Assert.AreEqual(_db.Waters.Find(1), _db.Houses.Find(1).Waters[0]);
        }
        [TestMethod]
        public void RegisterWaterWrongHouseId()
        {
            int waterId = 1;
            int houseId = 20;
            Assert.ThrowsException<KeyNotFoundException>(() => _waterService.RegisterWater(waterId, houseId));
        }
        [TestMethod]
        public void RegisterWaterWrongWaterId()
        {
            int waterId = 20;
            int houseId = 1;
            Assert.ThrowsException<KeyNotFoundException>(() => _waterService.RegisterWater(waterId, houseId));
        }
        [TestMethod]
        public void RegisterWaterDoubleChange()
        {
            int waterId = 1;
            int houseId = 1;
            _waterService.RegisterWater(waterId, houseId);
            Assert.ThrowsException<Exception>(() => _waterService.RegisterWater(waterId, houseId));
        }
    }
}
