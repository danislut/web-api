﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Models.DTO
{
    public class WaterReturnModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Укажите заводской номер")]
        [MaxLength(15, ErrorMessage = "Заводской номер не более 15 символов")]
        public string Number { get; set; }
        public uint Reading { get; set; }
        public int HouseId { get; set; }
    }
}
