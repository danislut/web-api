﻿using BL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Web_API
{
    public class HouseService2 : IHouseService
    {
        public DAL.Models.House CreateHouse(string address, string name)
        {
            throw new NotImplementedException();
        }

        public DAL.Models.House DeleteHouseById(int id)
        {
            throw new NotImplementedException();
        }

        public DAL.Models.House EditHouse(int id, string address, string name)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<DAL.Models.House> GetAllHouses()
        {
            throw new NotImplementedException();
        }

        public DAL.Models.House GetHouseById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<DAL.Models.House> GetHouses()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<DAL.Models.Water> GetHouseWaters(DAL.Models.House house)
        {
            throw new NotImplementedException();
        }

        public DAL.Models.House LeastConsumption()
        {
            throw new NotImplementedException();
        }

        public DAL.Models.House MostConsumption()
        {
            throw new NotImplementedException();
        }
    }
}
