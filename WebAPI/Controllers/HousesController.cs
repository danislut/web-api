﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.Interfaces;
using BL.Services;
using DAL.Models;
using DAL.Models.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HousesController : ControllerBase
    {
        private IHouseService _houseService;
        private IWaterService _waterSevice;
        public HousesController(IHouseService houseService, IWaterService waterSevice)
        {
            _houseService = houseService;
            _waterSevice = waterSevice;
        }

        [HttpPost("createhouse")]
        public IActionResult CreateHouse([FromBody] House house)
        {
            try
            {
                return Ok(_houseService.CreateHouse(house));
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        [HttpPut("edithouse")]
        public IActionResult EditHouseById(HouseEditModel model)
        {
            try
            {
                return Ok(_houseService.EditHouseById(model));
            }
            catch (Exception e)
            {
                return BadRequest(e + $"\r\nid: {model.HouseId}\r\naddress: {model.Address}\r\nname: {model.Name}");
            }
        }

        [HttpDelete("delhouse/{id}")]
        public IActionResult DeleteHouseById(int id)
        {
            try
            {
                return Ok(_houseService.DeleteHouseById(id));
            }
            catch (Exception e)
            {
                return BadRequest("Unhandled exception: " + e.ToString());
            }
        }

        [HttpGet]
        [Route("houses")]
        public IEnumerable<House> GetHouses()
        {
            return _houseService.GetAllHouses();
        }

        [HttpGet("gethouse/{id}")]
        public IActionResult GetHouseById(int id)
        {
            return new ObjectResult(_houseService.GetHouseById(id));
        }

        [HttpGet("gethw/{houseId}")]
        public IEnumerable<WaterReturnModel> GetHouseWaters(int houseId)
        {
            return _houseService.GetHouseWaters(houseId);
        }

        [HttpPost("inithouses")]
        public IActionResult Init()
        {
            return Ok(_houseService.Init());
        }

        [HttpPost("createwater")]
        public IActionResult CreateWater([FromBody] Water water)
        {
            try
            {
                return Ok(_waterSevice.CreateWater(water));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet("waters")]
        public IEnumerable<Water> GetAllWaters()
        {
            return _waterSevice.GetAllWaters();
        }

        [HttpPut]
        [Route("readingbyid/{id}/{reading}")]
        public IActionResult AddWaterById(int id, uint reading)
        {
            try
            {
                return Ok(_waterSevice.AddWaterDataById(id, reading));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPut("readingbynum")]
        public IActionResult AddWaterDataByNum([FromQuery]string num, [FromQuery]uint reading)
        {
            try
            {
                return Ok(_waterSevice.AddWaterDataByNum(num, reading));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPut("regwater")]
        public IActionResult RegisterWater(int waterId, int houseId)
        {
            try
            {
                return Ok(_waterSevice.RegisterWater(waterId, houseId));
            }
            catch (Exception e)
            {
                return BadRequest(e + $"\r\nWater id : {waterId}\r\nHouse id : {houseId}");
            }
        }

        [HttpGet("least")]
        public HouseWaterSumModel LeastConsumption()
        {
            return _houseService.LeastConsumption();
        }

        [HttpGet("most")]
        public HouseWaterSumModel MostConsumption()
        {
            return _houseService.MostConsumption();
        }
    }
}