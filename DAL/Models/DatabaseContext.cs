﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class DatabaseContext : DbContext
    {
        public DbSet<House> Houses { get; set; }
        public DbSet<Water> Waters { get; set; }
        public DatabaseContext()
        {
            Database.EnsureCreated();
        }
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<House>().HasIndex(h => h.Address).IsUnique();
            //modelBuilder.Entity<Water>().HasIndex(w => w.Number).IsUnique();
            //modelBuilder.Entity<Water>()
            //.HasOne(w => w.House)
            //.WithMany(h => h.Waters)
            //.OnDelete(DeleteBehavior.Cascade);
        }
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseNpgsql("Host=myserver;Username=mylogin;Password=mypass;Database=mydatabase");
        //}
    }
}
