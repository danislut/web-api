﻿using BL.Interfaces;
using DAL.Models;
using DAL.Models.DTO;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL.Services
{
    public class HouseService : IHouseService, IDisposable
    {
        DatabaseContext _db;
        bool disposed = false;
        public HouseService(DatabaseContext database)
        {
            _db = database;
        }
        public string CreateHouse(House house)
        {
            if (house == null)
                throw new NullReferenceException();
            if (!house.IsValid())
                throw new ArgumentNullException();
            if (_db.Houses
                .Where(a => a.Address == house.Address)
                .Select(h => h)
                .FirstOrDefault() != null)
                throw new ArgumentException("Дом с данным адресом уже зарегистрирован");
            _db.Houses.Add(house);
            return _db.SaveChanges() >= 1 ? $"House {house.Address} created" : throw new Exception($"No changes at CreateHouse(id = {house.Id})");
        }
        public string DeleteHouseById(int id)
        {
            House result = _db.Houses.Find(id);
            if (result == null)
                throw new KeyNotFoundException($"Id = {id} not found");
            _db.Houses.Remove(result);
            return _db.SaveChanges() >= 1 ? $"House {result.Address} deleted" : throw new Exception($"No changes at DeleteHouseById(id = {id})");
        }
        public string EditHouseById(HouseEditModel model)
        {
            if (model == null)
                throw new NullReferenceException();
            string result = $"Updated house id = {model.HouseId}";

            House house = _db.Houses.Find(model.HouseId);
            if (house == null)
                throw new KeyNotFoundException($"House with id = {model.HouseId} not found");
            if (model.Address != null && house.Address != model.Address)
            {
                house.Address = model.Address;
                result += $" address = {model.Address}";
            }
            if (model.Name != null && house.Name != model.Name)
            {
                house.Name = model.Name;
                result += $" name = {model.Name}";
            }
            return _db.SaveChanges() >= 1
                ? result
                : throw new Exception($"No changes at EditHouseById(id = {model.HouseId}, address = {model.Address}, name = {model.Name})");
        }

        public IEnumerable<House> GetAllHouses()
        {
            //var query = (from h in _db.Houses.Include(w => w.Waters)
            //             select h).ToList();
            var query = _db.Houses
                .AsNoTracking()
                .GroupJoin(
                    _db.Waters,
                    house => house.Id,
                    water => water.HouseId,
                    (h, w) => new { h, w }
                )
                .SelectMany(sm =>
                    sm.w.DefaultIfEmpty(),
                    (h, w) => new { h.h, w }
                )
                .GroupBy(
                    g => g.h,
                    g => g.w,
                    (key, w) => new { key, meters = w.ToList() }
                )
                .ToList()
                .Select(s => { s.key.Waters = s.meters; return s.key; });
            //JsonConvert.SerializeObject(query, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
            return query.ToList(); // query _db.Houses
        }

        public House GetHouseById(int id)
        {
            House house = _db.Houses.Find(id);
            if (house == null)
                throw new KeyNotFoundException($"House with id = {id} not found");
            return house;
        }
        
        public IEnumerable<WaterReturnModel> GetHouseWaters(int houseId)
        {
            if (_db.Houses.Find(houseId) == null)
                throw new KeyNotFoundException($"House id = {houseId} not found");
            var query = 
                (from house in _db.Houses.Join(
                    _db.Waters,
                    h => h.Id,
                    w => w.HouseId,
                    (h, w) => new WaterReturnModel { Id = w.Id, Number = w.Number, Reading = w.Reading, HouseId = h.Id } )
                 where house.HouseId == houseId
                 select house).ToList();
            return query;
        }
        public string Init()
        {
            if (_db.Houses.Any())
                return "Just initialized";
            List<House> result = new List<House>();
            for (int i = 4; i < 10; i++)
                result.Add(new House { Name = "Danfoss", Address = "Новоуличная, " + i });
            foreach (var h in result)
                _db.Add(h);
            _db.SaveChanges();
            return "Initialized";
        }
        public HouseWaterSumModel LeastConsumption()
        {
            var query =
                (from house in _db.Houses
                 join water in _db.Waters on house.Id equals water.HouseId into reedingSum
                 select new HouseWaterSumModel
                 {
                     HouseId = house.Id,
                     Address = house.Address,
                     Name = house.Name,
                     Sum = (ulong)reedingSum.Sum(r => r.Reading)
                 })
                 .OrderBy(sum => sum.Sum).FirstOrDefault();
            return query;
        }

        public HouseWaterSumModel MostConsumption()
        {
            var query =
                (from house in _db.Houses
                 join water in _db.Waters on house.Id equals water.HouseId into reedingSum
                 select new HouseWaterSumModel
                 {
                     HouseId = house.Id,
                     Address = house.Address,
                     Name = house.Name,
                     Sum = (ulong)reedingSum.Sum(r => r.Reading)
                 }).OrderByDescending(sum => sum.Sum).FirstOrDefault();
            return query;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
                _db.Dispose();
            disposed = true;
        }
        ~HouseService()
        {
            Dispose(false);
        }
    }
    //public class Response
    //{
    //    public bool Ok { get; set; }
    //    public string Message { get; set; }
    //}
}
