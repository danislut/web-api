﻿using BL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL.Services
{
    public class WaterService : IWaterService
    {
        DatabaseContext _db;
        bool disposed = false;
        public WaterService(DatabaseContext database)
        {
            _db = database;
        }
        public string AddWaterDataById(int id, uint reading)
        {
            Water result = _db.Waters.Find(id);
            if (result == null)
                throw new KeyNotFoundException($"Water meter with id = {id} not found");
            result.Reading = reading;
            return _db.SaveChanges() >= 1
                ? $"Water meter id = {id} updated (reading = {reading})"
                : throw new Exception($"No changes at AddWaterDataById(id = {id} reading = {reading}");
        }
        public string AddWaterDataByNum(string num, uint reading)
        {
            Water result = _db.Waters.FirstOrDefault(w => w.Number == num);
            if (result == null)
                throw new KeyNotFoundException($"Water meter with number = {num} not found");
            result.Reading = reading;
            return _db.SaveChanges() >= 1
                ? $"Water meter number = {num} updated (reading = {reading})"
                : throw new Exception($"No changes at AddWaterDataByNum(number = {num} reading = {reading}");
        }
        //public Water CreateWater(string num, int reading)
        //{
        //    if (num.Length > 15)
        //        throw new NotImplementedException("Слишком большая длина номера счетчика");
        //    if (reading < 0)
        //        throw new NotImplementedException("Показания счетчика не могут быть отрицательными");
        //    foreach (var w in _db.Waters)
        //        if (w.Number == num)
        //            throw new NotImplementedException("Данный серийный номер уже существует");
        //    Water result = new Water { Number = num, Reading = reading };
        //    _db.Waters.Add(result);
        //    _db.SaveChanges();
        //    return result;
        //}
        public string CreateWater(Water water)
        {
            if (water == null)
                throw new NullReferenceException();
            if (!water.IsValid())
                throw new ArgumentNullException();
            if (_db.Waters
                .Where(n => n.Number == water.Number)
                .Select(w => w)
                .FirstOrDefault() != null)
                throw new ArgumentException("Счетчик с данным номером уже зарегистрирован");
            _db.Waters.Add(water);
            return _db.SaveChanges() >= 1 
                ? $"Water meter {water.Number} created" 
                : throw new Exception($"No changes at CreateWater(id = {water.Id})");

        }

        public IEnumerable<Water> GetAllWaters()
        {
            return _db.Waters;
        }
        public string RegisterWater(int waterId, int houseId)
        {
            Water water = _db.Waters.Find(waterId);
            if (water == null)
                throw new KeyNotFoundException($"Water meter with id = {waterId} not found");
            House house = _db.Houses.Find(houseId);
            if (house == null)
                throw new KeyNotFoundException($"House with id = {houseId} not found");

            if (house.Waters == null)
                house.Waters = new List<Water>();
            house.Waters.Add(water);
            water.HouseId = houseId;
            water.House = house;
            return _db.SaveChanges() >= 1 ? "Water meter registered" 
                : throw new Exception($"No changes at CreateWater(id = { water.Id }");
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
                _db.Dispose();
            disposed = true;
        }
        ~WaterService()
        {
            Dispose(false);
        }
    }
}
