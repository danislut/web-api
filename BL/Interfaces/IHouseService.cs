﻿using DAL.Models;
using DAL.Models.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BL.Interfaces
{
    public interface IHouseService
    {
        // House CreateHouse(string address, string name);
        string CreateHouse(House house);
        string DeleteHouseById(int id);
        string EditHouseById(HouseEditModel model);
        IEnumerable<House> GetAllHouses();
        House GetHouseById(int id);
        IEnumerable<WaterReturnModel> GetHouseWaters(int houseId);
        string Init();
        HouseWaterSumModel LeastConsumption();
        HouseWaterSumModel MostConsumption();
    }
    public interface IWaterService
    {
        string AddWaterDataById(int id, uint reading);
        string AddWaterDataByNum(string num, uint reading);
        //Water CreateWater(string num, int reading);
        string CreateWater(Water water);
        IEnumerable<Water> GetAllWaters();
        string RegisterWater(int waterId, int houseId);
    }
}
