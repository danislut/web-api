﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Models.DTO
{
    public class HouseEditModel
    {
        [Required]
        public int HouseId { get; set; }
        public string Address { get; set; }
        public string Name { get; set; }
    }
}
