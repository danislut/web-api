﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models.DTO
{
    public class HouseReturnModel
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public string Name { get; set; }
        public List<Water> Waters { get; set; }
        //public List<string> HeatMeterSerialNumber { get; set; }
    }
}
