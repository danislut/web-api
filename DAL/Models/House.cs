﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Models
{
    public class House
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Введите адрес")]
        [Index(IsUnique = true)]
        public string Address { get; set; }
        public string Name { get; set; }
        public List<Water> Waters { get; set; }

        public bool IsValid ()
        {
            return this.Address != null;
        }

        //public override bool Equals(object obj)
        //{
        //    var a = obj as House;
        //    if (a == null)
        //        return false;
        //    return a.Address == this.Address && a.Name== this.Name;
        //}

        //public override int GetHashCode()
        //{
        //    return base.GetHashCode();
        //}
    }
}
